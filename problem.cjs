const fs = require("fs")
const path = require("path")


//  1. Retrieve data for ids : [2, 13, 23].

function retrieveData(ids) {

    return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {

        if (err) {
            err = new Error("error reading the file")
            console.log(err)
        } else {
            let employeesData = JSON.parse(data).employees.filter(data => {
                return data.id == ids
            })
            fs.writeFile(path.join(__dirname, "filteredIds.json"), JSON.stringify(employeesData), ((err, data) => {
                if (err) {
                    err = new Error("error writing a file")
                    console.log(err)
                } else {
                    console.log("files written")
                }
            }))

        }
    }))
}
// retrieveData(2)

function groupDataBasedOnCompanies(data) {
    return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {
        if (err) {
            err = new Error("error in reading the file")
        } else {
            let employeesGroupedOnCompanies = JSON.parse(data).employees.reduce((accumulator, current) => {
                let arr = []
                if (accumulator[current.company]) {
                    accumulator[current.company].push(current.name)
                } else {
                    accumulator[current.company] = arr
                }
                return accumulator
            }, {})
            fs.writeFile(path.join(__dirname, "groupedBasedOnCompany.json"), JSON.stringify(employeesGroupedOnCompanies), ((err, data) => {
                if (err) {
                    err = new Error("error in writing a file")
                    console.log(err)
                } else {
                    console.log("files written")
                }
            }))
        }
    }))
}
// groupDataBasedOnCompanies()

function getAllDataofPowerPuff(companyName) {

    return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {
        if (err) {

            err = new Error("error in reading the file")
        } else {
            let dataForPowerPuffCompany = JSON.parse(data).employees.filter(data => {
                return data.company.includes(companyName)

            })

            fs.writeFile(path.join(__dirname, "dataForPowerPuff.json"), JSON.stringify(dataForPowerPuffCompany), ((err, data) => {
                if (err) {
                    err = new Error("error in writing a file")
                    console.log(err)
                } else {
                    console.log("files written")
                }
            }))

        }

    }))
}
// getAllDataofPowerPuff("Powerpuff Brigade")



function removeId(ids) {
    return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {
        if (err) {
            err = new Error("error in reading the file")
        } else {
            let removeIdwithtwo = JSON.parse(data).employees.filter(data => {
                return data.id != ids
            })
            fs.writeFile(path.join(__dirname, "removedData.json"), JSON.stringify(removeIdwithtwo), ((err, data) => {
                if (err) {
                    err = new Error("error in writing a file")
                    console.log(err)
                } else {
                    console.log("files written")
                }
            }))
        }

    }))
}
// removeId(2)

function sortBasedOnComapnyName() {
    return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {
        if (err) {
            err = new Error("error in reading the file")
        } else {
            let sortBasedOnCompany = JSON.parse(data).employees.sort((companyNameA, companyNameB) => {
                if (companyNameB.company > companyNameA.company) {
                    return -1
                } else if (companyNameA.id == companyNameB.id) {
                    if (companyNameB.id > companyNameA.id) {
                        return -1
                    }
                }
            })
            fs.writeFile(path.join(__dirname, "sortBasedOnCompany.json"), JSON.stringify(sortBasedOnCompany), ((err, data) => {
                if (err) {
                    err = new Error("error in writing a file")
                    console.log(err)
                } else {
                    console.log("files written")
                }
            }))

        }

    }))
}
// sortBasedOnComapnyName()
function swapCompany(swap1, swap2) {
    fs.readFile(path.join(__dirname, "data.json"), (err, data) => {
        if (err) {
            throw new Error(err);

        } else {
            let resultJSON = JSON.parse(data)["employees"];
            let swap1data;
            let swap2data;
            for (let index = 0; index < resultJSON.length; index++) {
                if (resultJSON[index]["id"] === swap1) {
                    swap1 = index
                    swap1data = resultJSON[index];
                } else if (resultJSON[index]["id"] === swap2) {
                    swap2 = index
                    swap2data = resultJSON[index];
                }

            }
            resultJSON.splice(swap1, swap1, swap2data);
            resultJSON.splice(swap2, swap2, swap1data);
            fs.writeFile(path.join(__dirname, "swapCompany.json"), JSON.stringify(resultJSON), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })

        }
    })
}
// swapCompany(93, 92)

function addBirthdayIfIdIsEven() {
    return fs.readFile(path.join(__dirname, "data.json"), ((err, data) => {
        if (err) {
            err = new Error("error in reading the file")
        } else {
            let addBirthKey = JSON.parse(data).employees.map(data => {
                let date = Date()
                if (data.id % 2 == 0) {
                    data["birthday"] = date.split(' ').slice(1, 4)
                }
                return data
            })
            fs.writeFile(path.join(__dirname, "addBirthdayIfIdIsEven.json"), JSON.stringify(addBirthKey), ((err, data) => {
                if (err) {
                    err = new Error("error in writing a file")
                    console.log(err)
                } else {
                    console.log("files written")
                }
            }))
        }

    }))
}
addBirthdayIfIdIsEven()